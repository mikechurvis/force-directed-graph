﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphingLib
{
    /// <summary>
    ///     Class for constructing a simple unordered generic type graph.
    /// </summary>
    /// <typeparam name="TVertex">
    ///     The type of data to be stored in this graph.
    /// </typeparam>
    public class Graph<TVertex>
    {
        List<TVertex> vertexList;
        List<TVertex[]> edgeList;
        Dictionary<TVertex, List<TVertex>> adjacencyDict;

        const string 
            EDGE_NULL_VERTS_EX = "Edge cannot connect null vertices!",
            EDGE_SAME_VERTS_EX = "Edge vertices cannot be the same vertex!",
            EDGE_MISSING_VERTS_EX = "Because the parameter 'addMissingVerts' is false, the source and " +
            "target vertices you wish to connect must already exist inside the graph. If this is " +
            "undesired, consider allowing the default behaviour ('addMissingVerts = true').";



        /// <summary>
        ///     Constructs an empty graph.
        /// </summary>
        public Graph()
        {
            vertexList = new List<TVertex>();
            edgeList = new List<TVertex[]>();
            adjacencyDict = new Dictionary<TVertex, List<TVertex>>();
        }



        /// <summary>
        ///     Adds a given vertex to the graph.
        /// </summary>
        /// <param name="vertex">
        ///     The vertex to be added.
        /// </param>
        /// <returns>
        ///     The index of the newly added vertex.
        /// </returns>
        public int AddVertex (TVertex vertex)
        {
            int vertexIndex;

            //Add the vertex if it's not already in the graph.
            if (!vertexList.Contains(vertex))
            {
                vertexList.Add(vertex);
            }

            vertexIndex = vertexList.IndexOf(vertex);

            //Add space for adjacency

            return vertexIndex;
        }


        public void AddEdge (TVertex sourceVertex, TVertex targetVertex, bool bothWays = false, bool addMissingVertices = true)
        {
            TVertex[] newEdge;
            //Null vertices are not accepted.
            if (sourceVertex == null || targetVertex == null)
                throw new ArgumentNullException(EDGE_NULL_VERTS_EX);

            //Same vertex for both source and target is not accepted.
            if (sourceVertex.Equals(targetVertex))
                throw new ArgumentException(EDGE_SAME_VERTS_EX);

            //If no vertices are to be added, check that the supplied vertices already exist within the graph.
            if (!addMissingVertices)
            {
                if (!vertexList.Contains(sourceVertex) || !vertexList.Contains(targetVertex))
                    throw new InvalidOperationException(EDGE_MISSING_VERTS_EX);
            }
            
            //Build the new edge
            newEdge = new TVertex[] { sourceVertex, targetVertex };
            _BuildEdge(newEdge);

            //If the edge goes both ways, build an edge in the opposite direction
            if (bothWays)
            {
                newEdge[0] = targetVertex;
                newEdge[1] = sourceVertex;
                _BuildEdge(newEdge);
            }

            //Index adjacency


        }

        private void _BuildEdge (TVertex[] edge)
        {
            //If no edge sequentially identical to the new edge exists within the graph, add the new edge to the graph.
            if (!edgeList.Exists((e) => { return e.SequenceEqual(edge); }))
            {
                edgeList.Add(edge);
            }
        }
    }
}
